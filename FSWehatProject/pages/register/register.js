// pages/register/register.js.js
Page({

  /**
   * 用户点击提交
   */
  subbmitUserInfo: function (e) {
    e.detail.value.sessionCode = wx.getStorageSync('sessionCode')
    console.log(wx.getStorageSync('sessionCode') + "用户完善信息：" + JSON.stringify(e.detail.value))
    if (e.detail.value.realName == '') {
      wx.showModal({
        title: '提示',
        content: '请填写手机号',
      })
      return
    }
    if (e.detail.value.phoneNum == '') {
      wx.showModal({
        title: '提示',
        content: '请填写手机号',
      })
      return
    } else if (e.detail.value.phoneNum.length != 11) {
      wx.showModal({
        title: '提示',
        content: '请填写正确的手机号',
      })
      return
    }
    if (e.detail.value.qq == '') {
      wx.showModal({
        title: '提示',
        content: '请填写QQ号',
      })
      return
    }
    wx.request({
      url: "http://192.168.1.101/register/subbmit.do",
      data: e.detail.value,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function (result) {
        console.log("请求结果：" + JSON.stringify(result))
        if (result.data.code == "100") {
          console.log("用户补充信息成功，跳转首页")
          wx.switchTab({
            url: '../index/index',
          })
        } else {
          console.log("用户补充信息失败：" + result.data.msg)
          wx.showModal({
            title: '提示',
            content: '提交失败，请重试',
          })
        }
      }
    })
  },

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})