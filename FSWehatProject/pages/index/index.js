//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    //userInfo: {},
    //hasUserInfo: false,
    //canIUse: wx.canIUse('button.open-type.getUserInfo'),
    // orderNum:'', // 订单号
    // expressNum:'', // 物流单号
    // expressUserName:'', // 物流单上名称
    // expressName:'', // 物流名称
    // expressPhone:'', // 物流手机号
    // productName:'', // 物品名称（描述）
    // productNum:'', // 物品数量
    // selfPayAccount:'', // 自付金额
    // resuAccount:'', // 结算金额
    // isProductPay:0, // 是否货到付款
    // isSelfPay:0, // 是否自行垫付
    // user_code:'', // 用户系统编码
    // remark:'', // 备注
    // productType:0, // 物品类型（是否为实物）
    // employedType:0 // 产品属性
  },
  
  onLoad: function () {
    //getUserInfo();
    
    console.log("开始加载首页。。。。");
  },

  // // 监听订单号
  // listenerOrderNum: function(e) {
  //   this.orderNum = e.detail.value;
  // },

  // // 监听物流用户名称
  // listenerExpressUserName:function(e) {
  //   this.expressUserName = e.detail.value;
  // },

  // // 监听物流手机号
  // listenerExpressPhoneNum:function(e) {
  //   this.expressPhone = e.detail.value;
  // },

  // // 监听产品名称
  // listenerProductName:function(e) {
  //   this.productName = e.detail.value;
  // },

  // // 监听物品数量
  // listenerProductNum:function(e) {
  //   this.productNum = e.detail.value;
  // },

  // // 监听自付金额
  // listenerSelfPay:function(e) {
  //   this.selfPayAccount = e.detail.value;
  // },


  // // 监听结算金额
  // listenerResultAccount:function(e) {
  //   this.resuAccount = e.detail.value;
  // },

  // // 监听物品类型
  // listenerproductType:function(e) {
  //   this.productType = e.detail.value;
  // },

  // // 监听是否货到付款
  // listenerIsProductPay:function(e) {
  //   this.isProductPay = e.detail.value;
  // },

  // // 监听货品属性
  // listenerEmployedType:function(e) {
  //   this.employedType = e.detail.value;
  // },

  // listenerRemark:function(e) {
  //   this.remark = e.detail.value;
  // },

  // 监听提交订单信息
  submitUserOrderInfos:function(e) {
    e.detail.value.sessionCode = wx.getStorageSync('sessionCode');
    console.log(wx.getStorageSync('sessionCode') + "用户录单信息：" + JSON.stringify(e.detail.value));
    if (e.detail.value.orderNum == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证订单号（物流单号）不为空',
      })
      return;
    }

    if (e.detail.value.expressUserName == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证物流单上姓名不为空',
      })
      return;
    }

    if (e.detail.value.phoneNum == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证物流单上手机号不为空',
      })
      return;
    } else if (e.detail.value.phoneNum.length != 11) {
      wx.showModal({
        title: '提示',
        content: '请填写正确的手机号',
      })
      return;
    }

    if (e.detail.value.productName == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证物品名称（描述）不为空',
      })
      return;
    }

    if (e.detail.value.productNum == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证物品数量不为空',
      })
      return;
    }

    if (e.detail.value.settlement == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证结算金额不为空',
      })
      return;
    }
    if (e.detail.value.isProductPay == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证签收方式不为空',
      })
      return;
    }
    if (e.detail.value.employedType == '') {
      wx.showModal({
        title: '参数缺失',
        content: '请保证平台类型不为空',
      })
      return;
    }

    wx.request({
      url: 'http://192.168.1.101/userOrderInfos/addUserOrderInfo',
      data: e.detail.value,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function (result) {
        console.log("请求结果：" + JSON.stringify(result));
        if (result.data.code == "200") {
          console.log("用户录入成功，跳转首页")
          wx.showModal({
            title: '提示',
            content: '录入成功',
          })
          wx.switchTab({
            url: '../index/index',
          })
        } else {
          if(result.data.code=="201") {
            console.log("用户录入的订单已经存在");
            wx.showModal({
              title: '警告',
              content: result.data.msg,
            })
            wx.switchTab({
              url: '../index/index',
            })
          } else if (result.data.code =="102") {
            wx.showModal({
              title: '警告',
              content: '录入已过期，录入失败，请重新登录后录入',
            })
            wx.switchTab({
              url: '../index/index',
            })
          }
        }
      }
    })
  },

  // getUserInfo: function(e) {
  //   console.log(e)
  //   app.globalData.userInfo = e.detail.userInfo
  //   this.setData({
  //     userInfo: e.detail.userInfo,
  //     hasUserInfo: true
  //   })
  // }
})
