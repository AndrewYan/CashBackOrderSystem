//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log(res.code);
        wx.getSetting({
          success(setRes) {
            // 判断是否已授权  
            if (!setRes.authSetting['scope.userInfo']) {
              // 授权访问
              wx.authorize({
                scope: 'scope.userInfo',
                success(){
                  // 获取用户信息
                  wx.getUserInfo({
                    lang: "zh_CN",
                    success:function(userRes) {
                      wx.request({
                        url: "http://192.168.1.101/login/check.do",
                        data: {
                          jsCode: res.code,
                          encryptedData: userRes.encryptedData,
                          iv: userRes.iv
                        },
                        header: {
                          "Content-Type": "application/x-www-form-urlencoded"
                        },
                        method: 'POST',
                        success: function (result) {
                          console.log("请求结果：" + JSON.stringify(result))
                          wx.setStorageSync("sessionCode", result.data.data.sessionCode);
                          if (result.data.code == "101") {
                            console.log("跳转补充信息页面，让用户补充信息")
                            wx.redirectTo({
                              url: '../register/register',
                            })
                          } else {
                            console.log("登录成功，跳转首页")
                            wx.switchTab({
                              url: '../index/index',
                            })
                          }
                        }
                      })
                    }
                  })
                }
              })
            } else {
              console.log("已授权，开始获取用户信息，接着登录");
              wx.getUserInfo({
                lang: "zh_CN",
                success: function (userRes) {
                  console.log("获取用户信息成功，开始发起网络请求登录");
                  wx.request({
                    url: "http://192.168.1.101/login/check.do",
                    data: {
                      jsCode: res.code,
                      encryptedData: userRes.encryptedData,
                      iv: userRes.iv
                    },
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    method: 'POST',
                    success: function (result) {
                      console.log("请求结果：" + JSON.stringify(result))
                      wx.setStorageSync("sessionCode", result.data.data.sessionCode);
                      if (result.data.code == "101") {
                        console.log("跳转补充信息页面，让用户补充信息")
                        wx.redirectTo({
                          url: '../register/register',
                        })
                      } else {
                        console.log("登录成功，跳转首页")
                        wx.switchTab({
                          url: '../index/index',
                        })
                      }
                    }
                  })
                }
              })
            }
          }
        })
      }
    })
    // 获取用户信息
    // wx.getSetting({
    //   success: res => {
    //     if (res.authSetting['scope.userInfo']) {
    //       // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
    //       wx.getUserInfo({
    //         success: res => {
    //           // 可以将 res 发送给后台解码出 unionId
    //           this.globalData.userInfo = res.userInfo

    //           // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
    //           // 所以此处加入 callback 以防止这种情况
    //           if (this.userInfoReadyCallback) {
    //             this.userInfoReadyCallback(res)
    //           }
    //         }
    //       })
    //     }
    //   }
    // })
  },
  globalData: {
    userInfo: null,
    sessionId:null,
    userId: null

  }
})